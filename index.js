let assets = [
	{
		id: "kingofthejungle",
		name: "Liono",
		description: "apex-predator",
		stock: 100,
		isAvailable: true,
		dateAdded: 1/1/2022
	},

	{
		id: "fastgirl",
		name: "Cheetarah",
		description: "fastest cat",
		stock: 70,
		isAvailable:true,
		dateAdded: 10/10/2021
	}
]

console.log(assets)


/*JSON -Javascript Object Notation

	JSON is a string formatted as JS Objecct.
	JSON is popularly used to pass data from one application to another.
	Strings are easier to transfer from application to another
	JSON is a string.
	JS Object is an object.
	JSON keys are surrounded by double quotes.
	an extra comma at the end of the object in JSON makes it an error
	*/


	let jsonSample = `{

		"samplekey1": "valueA",
		"samplekey2": "valueB",
		"samplekey3": 1,
		"samplekey4": true
		
	}`

	console.log(typeof jsonSample);

	let jsonConvert = JSON.parse(jsonSample);
	console.log(typeof jsonConvert);
	console.log(jsonConvert);


	//JSON is not only usded in JS but also in other programming languages.
	//This is why it is specified as JavaScript Object Notation.
	//There are also JSON that are save in a file with extension called .json

	//There's also a way to turn JSON into JS Objects and turn JS Objects into JSON
	//Converting JS OBjects into JSON

		//This will allow us to convert/turn JS Objects into JSON
		//This is how JSON is manipulated even when we received by other applications.


		let batches = [

			{
				batch: 'Batch 152'
			},
			{
				batch: 'Batch 156'
			}

		];

		//To turn JS Objeccts into JSON we use the method JSON.stringify().
		//This method will return JSON format out of the object that we pass as an 
		// argument.

		let batchesJSON = JSON.stringify(batches);
		console.log(batchesJSON);


		let data = {
			name: "Katniss",
			age: 20,
			address: {
				city:"Kansas City",
				state: "Kansas"
			}
		}

		let dataJSON = JSON.stringify(data);
		console.log(dataJSON);

		/*JSON.stingify is commonly used when trying to pass data from one application toanother via HTTP reques
		Http requests are requests for datat between client (browser/page/app) and a server.

*/
		

		let data2 = {
			username: "saitamaOPM",
			password: "onepuuuuunch",
			isAdmin: true
		}

		let data3 = {
			username: "lightYagami",
			password: "notKiraDefinitely",
			isAdmin: false
		}

		let data4 = {
			username: "Llawliett", 
			password: "yagamiiskira07", 
			isAdmin: false 
		}


//mini-activity

	let data2JSON = JSON.stringify(data2);
	let data3JSON = JSON.stringify(data3);
	let data4JSON = JSON.stringify(data4);

	console.log(data2JSON);
	console.log(data3JSON);
	console.log(data4JSON);

	let jsonArray = JSON.stringify(assets);

	console.log(jsonArray);

	//jsonArray.pop()
	//cant use pop because JSON is a string



	let items = `[

			{

				"id": "shop-1",
				"name": "Oreos",
				"stock": 5,
				"price": 50
			},
			{
				"id": "shop-2",
				"name": "Doritos",
				"stock": 10,
				"price": 150

			}

	]`

	let itemsJSArr = JSON.parse(items);
	console.log(itemsJSArr);


	itemsJSArr.pop();
	console.log(itemsJSArr);

	//JSON.stringify() the itemsJSArr and re-assign to items:
	items = JSON.stringify(itemsJSArr);
	console.log(items);

	let courses = `[

			{
				"name:" "Math 101",
				"description": "learn the basics of Math.",
				"price": 2500

			},
			{
				"name": "Science 101",
				"description": "learn the basics of Science",
				"price": 2500

			}

	]`


let coursesJSArr = JSON.parse(courses);
console.log(coursesJSArr);

coursesJSArr.pop();
console.log(coursesJSArr);
courses = JSON.stringify(coursesJSArr);
console.log(courses);

let coursesJSArr2 = JSON.parse(courses);

coursesJSArr2.push({
	name: "Science 101",
	description: "learn the basics of Science",
	price: 2500
})

let coursesJSArr2 = JSON.stringify(courses);
console.log(coursesJSArr2)






























