//Objects can also be grouped into an array
//all array methods will also be implemented.

let users = [

	{
		username: "mike9900",
		email: "michaeljames@gmail.com",
		password: "mikecutie1999",
		isAdmin: false

	},
	{
		username: "justin89",
		email: "justinlake@gmail.com",
		password: "iamsync00",
		isAdmin: true
	}


];

//How can we access items in an array of objects?

console.log(users[0]);
//just like how we accessed items in an array. we get the name of the array
//and then access the index

console.log(users[1]);

//log the email property
console.log(users[1].email);

//log the username property 

console.log(users[0].username);

//method used in the object array to add a new user object


users.push({

	username: "abbeyarcher",
	email: "abbeyarcher@gmail.com",
	password: "bowandabbey",
	isAdmin: false

})

console.log(users);

let newUser = {
	username: "hanna1993",
	email: "hannafight@gmail.com",
	password: "fighting1993",
	isAdmin: false,
}

users.push(newUser);

console.log(users);

class User {
	constructor(username,email,password){
		this.username = username;
		this.email = email;
		this.password = password;
		this.isAdmin = false;
	}
}

let user1 = new User("kateduchess","nottherealone@gmail.com","notroyalty");
users.push(user1);

console.log(users);


/* Mini activity*/


let user2 = new User("williamduke","nottherealduke@gmail.com","alsonotroyalty");
users.push(user2);

console.log(users);

users.push({

	username: "harrydilf",
	email: "nottherealharry@gmail.com",
	password: "royaltyinmyhead",
	isAdmin: false
})

console.log(users);
//---------------------------------------
users.push(new User("newuser1","newuseremail@gmail.com","newuser8"));

console.log(users)

//register function

	//tip: when creating a function which will receive data as an argument,
	//always console.log the parameters first.


	function register(username,email,password){
		///console.log(username);
		//console.log(email);
		//console.log(password);
	//register function should be able to push a new user objetion our
	//array
		if (username.length < 8 || email.length < 8 || password.length < 8 ){
			alert("Details provided too short. username, email, password must be more than 8 characters.")
			} else {

						users.push({
			
							username: username,
							email: email,
							password: password,
							isAdmin: false

						})


			}

	
	}

register("jeffrey1888","immortal1@gmail.com","awesomemortal1");


console.log(users)


//login function
// find() - check if our arrAY IF THERE IS AUSER WHICH MATHES THE EMAIL AND PASSWORD PROVIDED


//find() is like forEach and map , it will iterate/loop over each item in the array and then return a condition. If the condition returned result to true, the find() method will return the item that is currenlty iterated or looped over.

function login(emailInput,pwInput){

	//console.log(emailInput);
	//console.log(pwInput);

	//anonymous function in the find() method recieves the current item being looped over or iterated.

	let foundUser = users.find((user)=>{

			console.log(user.email);
			console.log(user.password);

			return user.email === emailInput && user.password === pwInput;

	})

	//console.log(foundUser);

	if(foundUser !== undefined){
		console.log("Thank you for logging in.")
	} else {
		console.log("Invalid Credentials. No User Found.")
	}


}

login("immortal1@gmail.com","awesomemortal1")









